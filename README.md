# Challenge

### Sobre:
Este desafio foi feito para suprir o solicitado em: https://github.com/itidigital/platform-challenge/

O Desafio era containerizar uma aplicação em Kotlin utilizando a versão 4.10 do Gradle que mostra um "Hello World!" ao receber um CURL na porta 8080

Para containerizar a aplicação, foi criado um Dockerfile que está na raiz do projeto contendo as especificações necessárias e também utilizando a versão correta do Gradle, bem como demais dependencias para funcionamento correto da aplicação

### Helm Charts
Foi criado um helm chart básico para que seja possível instalar as dependencias em um Cluster de Kubernetes de forma simples

### Pipeline
Foi criado uma Pipeline (.gitlab-ci.yml) que possui o seguinte objetivo:
- Build da Aplicação (Criando uma imagem docker que é hospedada no Docker Hub)
- Provisionamento de Infra utilizando Amazon EKS
- Aplicação do Helm Chart no Cluster EKS

### Infra
A infra foi provisionada com Terraform para criação de um Cluster Kubernetes com 3 nós na região de São Paulo (sa-east-1)

### Como funciona 
Ao receber um commit na branch MASTER, a pipeline é disparada e executa alguns stages de forma automática e outros são disparados manualmente (terraform apply e terraform destroy), possui essa caracteristica para garantir que tudo possa ser revisado antes de ser aplicado e destruido e também pelo potencial destrutivo desses comandos

O build da aplicação requer a existencia de um Dockerfile na raiz do projeto, caso contrário o build da imagem não é executado e ele utilizara a imagem LATEST disponível no DockerHub na hora da aplicação do Helm Chart

A pipeline por hora esta separada em: build-docker (requer Dockerfile na raiz), init (automatico), validate (automatico), plan (automatico), apply (manual), destroy (manual), install-helm-chart (manual) e uninstall-helm-chart (manual)
